package ru.zejaven.practice_hall;

import java.util.Arrays;

public class NumbersSum {
    public static void main(String[] args) {
        String str = "22 48 10 5 2";
        System.out.println(sum(str));
    }

    private static int sum(String str) {
        return Arrays.stream(str.split(" ")).map(Integer::parseInt).reduce(Integer::sum).orElse(0);
    }
}
