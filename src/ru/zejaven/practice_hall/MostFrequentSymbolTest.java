package ru.zejaven.practice_hall;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class MostFrequentSymbolTest {

    public static void main(String[] args) {
        String s = "и это\n" +
                " \n" +
                "как найти самый частовстречаемый символ в строке стримом?\n" +
                "Stas\n" +
                "Stas 5:49 pm\n" +
                " \n" +
                "Dmitry Sementsov\n" +
                "string.replaceAll(\"[ ]\", \"\").length();\n" +
                "а нафига квадратные скобки?\n" +
                " \n" +
                "вроде не нужны\n" +
                "Dmitry\n" +
                "Dmitry 5:53 pm\n" +
                " \n" +
                "Но это сработало\n" +
                "Stas\n" +
                "Stas 5:56 pm\n" +
                " \n" +
                "ну да\n" +
                " \n" +
                "потому что так можно\n" +
                " \n" +
                "но не нужно\n" +
                " \n" +
                "можно просто пробел\n" +
                " \n" +
                "string.replaceAll(\" \", \"\").length();\n" +
                "Dmitry\n" +
                "Dmitry 5:57 pm\n" +
                " \n" +
                "оки\n" +
                "Stas\n" +
                "Stas 5:57 pm\n" +
                " \n" +
                "или string.replaceAll(\"\\s\", \"\").length();\n" +
                " \n" +
                "так ты еще переводы строк уберешь\n" +
                " \n" +
                "и табы\n" +
                " \n" +
                "вроде\n" +
                "Stas\n" +
                "Stas 5:59 pm\n" +
                " \n" +
                "Dmitry Sementsov\n" +
                "у тебя есть обоснованное мнение что лучше и почему?\n" +
                "хуй знает\n" +
                "Dmitry\n" +
                "Dmitry 6:02 pm\n" +
                " \n" +
                "тоже ответ)\n" +
                " \n" +
                "дальше\n" +
                "Stas\n" +
                "Stas 6:45 pm\n" +
                " \n" +
                "Dmitry Sementsov\n" +
                "как найти самый частовстречаемый символ в строке стримом?\n" +
                "а, чет я пропустил\n" +
                " \n" +
                "хмммм\n" +
                "Stas\n" +
                "Stas 7:34 pm\n" +
                " \n" +
                "private static Set<Character> getMostFrequentSymbols(String s) {\n" +
                "return s.chars().boxed()\n" +
                ".collect(Collectors.groupingBy(c -> c, Collectors.counting()))\n" +
                ".entrySet().stream()\n" +
                ".collect(Collectors.groupingBy(\n" +
                "Map.Entry::getValue,\n" +
                "Collectors.collectingAndThen(\n" +
                "Collectors.toSet(),\n" +
                "setOfMap -> setOfMap.stream().map(map -> (char) map.getKey().intValue()).collect(Collectors.toSet())\n" +
                ")\n" +
                ")).entrySet().stream()\n" +
                ".reduce((a, b) -> b.getKey().compareTo(a.getKey()) > 0 ? b : a)\n" +
                ".orElseThrow(IllegalArgumentException::new)\n" +
                ".getValue();\n" +
                "}\n" +
                " \n" +
                " \n" +
                "там их несколько я возвращаю\n" +
                " \n" +
                "потому что разные символы могут встречаться одинаковое количество раз\n" +
                "Dmitry\n" +
                "Dmitry 7:55 pm\n" +
                " \n" +
                " \n" +
                "Stas\n" +
                "Stas 7:58 pm\n" +
                " \n" +
                "ля\n" +
                " \n" +
                "а нахуя я тогда в character обратно переводил\n" +
                " \n" +
                "если нужен байт\n" +
                " \n" +
                "ну у тебя все равно длиньше\n" +
                " \n" +
                "\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\n" +
                "Dmitry\n" +
                "Dmitry 7:59 pm\n" +
                " \n" +
                "нихуя\n" +
                "Stas\n" +
                "Stas 7:59 pm\n" +
                " \n" +
                "я запушил, кстати\n" +
                "Dmitry\n" +
                "Dmitry 8:00 pm\n" +
                " \n" +
                "Stas\n" +
                "Stas 8:04 pm\n" +
                " \n" +
                "ахах\n" +
                " \n" +
                "нихуя се\n" +
                " \n" +
                "хитро сделал\n" +
                " \n" +
                "всего типа 256 символов\n" +
                " \n" +
                "и типа 256 счетчиков\n" +
                " \n" +
                "которые прибавляются\n" +
                " \n" +
                "если встречается символ на месте индекса, соответствующего коду символа \n" +
                " \n" +
                "ну да, решать в стримах такое, полагаю, излишне\n" +
                "Dmitry\n" +
                "Dmitry 8:09 pm\n" +
                " \n" +
                "я просто ожидал что-то такое \".collect(Collectors.groupingBy(c -> c, Collectors.counting())\"\n" +
                "Stas\n" +
                "Stas 8:12 pm\n" +
                " \n" +
                "хммм, ну может и можно как-то короче\n" +
                " \n" +
                "но я делал первое, что приходило в голову\n" +
                "Dmitry\n" +
                "Dmitry 8:13 pm\n" +
                " \n" +
                "охуеть у тебя каша в голове:D\n" +
                "Stas\n" +
                "Stas 8:15 pm\n" +
                " \n" +
                "\uD83D\uDC38\uD83D\uDC38\uD83D\uDC38\n" +
                "Stas\n" +
                "Stas 8:24 pm\n" +
                " \n" +
                "и это\n" +
                " \n" +
                "хватит \"о\"кать\n" +
                " \n" +
                " \n" +
                "за сегодняшний день было слишком много \"о\"\n" +
                "Dmitry\n" +
                "Dmitry 8:25 pm\n" +
                " \n" +
                "\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\n" +
                " \n" +
                "Да, довай, можно и не так много о \n" +
                "Stas\n" +
                "Stas 8:26 pm\n" +
                " \n" +
                "\uD83E\uDD23\uD83E\uDD23\uD83E\uDD23\n" +
                " \n" +
                "чет ору \uD83E\uDD23\n" +
                "Dmitry\n" +
                "Dmitry 8:30 pm\n" +
                " \n" +
                "\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\uD83D\uDE04\n" +
                " \n" +
                "Мой метод все равно лучше\n" +
                " \n" +
                "Он работает с потоками\n" +
                "Stas\n" +
                "Stas 8:33 pm\n" +
                " \n" +
                "мой лучше, его можно записать в одну строчку\n" +
                " \n" +
                "хоть он и не записан в одну строчку\n" +
                " \n" +
                "\uD83D\uDE01\n" +
                "Stas\n" +
                "Stas 8:41 pm\n" +
                " \n" +
                "а еще в нем можно переписать всего 2 строчки\n" +
                " \n" +
                " \n" +
                "и получить метод, который ищет чаще всего встречающиеся слова";
        String inputWOSpaces = s.replaceAll("\\s", "");
        String stringToCheckForSymbols = removeVKNamesAndTime(inputWOSpaces);
        String stringToCheckForWords = removeVKNamesAndTime(s);
        Set<Character> mostFrequentSymbols = getMostFrequentSymbols(stringToCheckForSymbols);
        Set<String> mostFrequentWords = getMostFrequentWords(stringToCheckForWords);
        System.out.println(stringToCheckForSymbols);
        System.out.println(mostFrequentSymbols);
        System.out.println(mostFrequentWords);
    }

    private static String removeVKNamesAndTime(String s) {
        return s.replaceAll("Stas", "")
                .replaceAll("Dmitry", "")
                .replaceAll("[\\d]{1,2}:[\\d]{1,2}\\s?[ap]m", "");
    }

    private static Set<Character> getMostFrequentSymbols(String s) {
        return s.chars().boxed()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .entrySet().stream()
                .collect(Collectors.groupingBy(
                        Map.Entry::getValue,
                        Collectors.collectingAndThen(
                                Collectors.toSet(),
                                setOfMap -> setOfMap.stream().map(map -> (char) map.getKey().intValue()).collect(Collectors.toSet())
                        )
                )).entrySet().stream()
                .reduce((a, b) -> b.getKey().compareTo(a.getKey()) > 0 ? b : a)
                .orElseThrow(IllegalArgumentException::new)
                .getValue();
    }

    private static Set<String> getMostFrequentWords(String s) {
        return Arrays.stream(s.split("\\s+"))
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()))
                .entrySet().stream()
                .collect(Collectors.groupingBy(
                        Map.Entry::getValue,
                        Collectors.collectingAndThen(
                                Collectors.toSet(),
                                setOfMap -> setOfMap.stream().map(Map.Entry::getKey).collect(Collectors.toSet())
                        )
                )).entrySet().stream()
                .reduce((a, b) -> b.getKey().compareTo(a.getKey()) > 0 ? b : a)
                .orElseThrow(IllegalArgumentException::new)
                .getValue();
    }
}
