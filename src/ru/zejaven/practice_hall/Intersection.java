package ru.zejaven.practice_hall;

import java.util.*;

public class Intersection {

    public static void main(String[] args) {
        System.out.println(
                Math.round(getPolygonIntersectionArea(5, 4, Arrays.asList(1, 1, 1, 3, 3, 4, 5, 2, 4, 0, 3, 3, 3, 1, 0, 0, -1, 4)))
        );
    }

    private static double getPolygonIntersectionArea(int N, int M, List<Integer> coords) {
        List<Double> xCoords1 = new ArrayList<>();
        List<Double> yCoords1 = new ArrayList<>();
        List<Double> xCoords2 = new ArrayList<>();
        List<Double> yCoords2 = new ArrayList<>();
        List<Point> points1 = new ArrayList<>();
        List<Point> points2 = new ArrayList<>();
        for (int i = 0; i < 2 * N; i+=2) {
            xCoords1.add((double) (int) coords.get(i));
            yCoords1.add((double) (int) coords.get(i + 1));
        }
        for (int i = 2 * N; i < 2 * N + 2 * M; i+=2) {
            xCoords2.add((double) (int) coords.get(i));
            yCoords2.add((double) (int) coords.get(i + 1));
        }
        for (int i = 0; i < N; i++) {
            points1.add(new Point(xCoords1.get(i), yCoords1.get(i)));
        }
        for (int i = 0; i < M; i++) {
            points2.add(new Point(xCoords2.get(i), yCoords2.get(i)));
        }
        Polygon polygon1 = createPolygon(points1);
        Polygon polygon2 = createPolygon(points2);
        Polygon intersection = getPolygonIntersection(polygon1, polygon2);
        List<Triangle> triangles = splitPolygonToTriangles(intersection);
        return triangles.stream().map(Triangle::getArea).reduce(Double::sum).orElse(0D);
    }

    private static List<Triangle> splitPolygonToTriangles(Polygon polygon) {
        List<Triangle> triangles = new ArrayList<>();
        for (int i = 1; i < polygon.getLines().size() - 1; i++) {
            triangles.add(new Triangle(
                    new Line(polygon.getLines().get(0).getP1(), polygon.getLines().get(i).getP1()),
                    polygon.getLines().get(i),
                    new Line(polygon.getLines().get(0).getP1(), polygon.getLines().get(i).getP2())
            ));
        }
        return triangles;
    }

    private static Polygon createPolygon(List<Point> points) {
        sortPoints(points);
        List<Line> lines = new ArrayList<>();
        for (int i = 0; i < points.size() - 1; i++) {
            lines.add(new Line(points.get(i), points.get(i + 1)));
        }
        lines.add(new Line(points.get(points.size() - 1), points.get(0)));
        return new Polygon(lines);
    }

    private static void sortPoints(List<Point> points) {
        Point startingPoint = getStartingPoint(points);
        points.remove(startingPoint);
        for (int i = 0; i < points.size(); i++) {
            for (int j = points.size() - 1; j > i; j--) {
                double angle1 = Math.atan((points.get(j - 1).getY() - startingPoint.getY()) / (points.get(j - 1).getX() - startingPoint.getX()));
                angle1 = angle1 < 0 ? Math.abs(angle1) : Math.PI - angle1;
                double angle2 = Math.atan((points.get(j).getY() - startingPoint.getY()) / (points.get(j).getX() - startingPoint.getX()));
                angle2 = angle2 < 0 ? Math.abs(angle2) : Math.PI - angle2;
                if (Double.compare(angle1, angle2) > 0) {
                    Point temp = points.get(j - 1);
                    points.set(j - 1, points.get(j));
                    points.set(j, temp);
                }
            }
        }
        points.add(0, startingPoint);
    }

    private static Point getStartingPoint(List<Point> points) {
        Point minY = points.stream().min(Comparator.comparing(Point::getY, Double::compareTo)).orElse(points.get(0));
        return points.stream()
                .filter(p -> Double.compare(p.getY(), minY.getY()) == 0)
                .min(Comparator.comparing(Point::getX, Double::compareTo))
                .orElse(points.get(0));
    }

    private static Point getLineIntersection(Line l1, Line l2) {
        double k = (
                (l2.getP2().getX() - l2.getP1().getX()) * (l1.getP1().getY() - l2.getP1().getY())
                        - (l2.getP2().getY() - l2.getP1().getY()) * (l1.getP1().getX() - l2.getP1().getX())
        ) / (
                (l2.getP2().getY() - l2.getP1().getY()) * (l1.getP2().getX() - l1.getP1().getX())
                        - (l2.getP2().getX() - l2.getP1().getX()) * (l1.getP2().getY() - l1.getP1().getY())
        );
        double x1 = l1.getP1().getX() + k * (l1.getP2().getX() - l1.getP1().getX());
        double y1 = l1.getP1().getY() + k * (l1.getP2().getY() - l1.getP1().getY());
        Point intersectionPoint = new Point(x1, y1);
        return canLineIncludePoint(l1, intersectionPoint) && canLineIncludePoint(l2, intersectionPoint)
                ? intersectionPoint
                : null;
    }

    private static boolean canLineIncludePoint(Line line, Point point) {
        double xMin = Math.min(line.getP1().getX(), line.getP2().getX());
        double xMax = Math.max(line.getP1().getX(), line.getP2().getX());
        double yMin = Math.min(line.getP1().getY(), line.getP2().getY());
        double yMax = Math.max(line.getP1().getY(), line.getP2().getY());
        return Double.compare(point.getX(), xMin) >= 0
                && Double.compare(point.getX(), xMax) <= 0
                && Double.compare(point.getY(), yMin) >= 0
                && Double.compare(point.getY(), yMax) <= 0;
    }

    private static Polygon getPolygonIntersection(Polygon polygon1, Polygon polygon2) {
        Set<Point> polygonPoints = new HashSet<>();
        for (int i = 0; i < polygon1.getLines().size(); i++) {
            for (int j = 0; j < polygon2.getLines().size(); j++) {
                Point point = getLineIntersection(polygon1.getLines().get(i), polygon2.getLines().get(j));
                if (point != null) {
                    polygonPoints.add(point);
                }
            }
        }
        for (int i = 0; i < polygon1.getLines().size(); i++) {
            for (int j = 0; j < polygon2.getLines().size(); j++) {
                Point lineFirstPoint = polygon2.getLines().get(j).getP1();
                if (isPointBelongsToPolygon(polygon1, lineFirstPoint)) {
                    polygonPoints.add(lineFirstPoint);
                }
            }
        }
        for (int i = 0; i < polygon2.getLines().size(); i++) {
            for (int j = 0; j < polygon1.getLines().size(); j++) {
                Point lineFirstPoint = polygon1.getLines().get(j).getP1();
                if (isPointBelongsToPolygon(polygon2, lineFirstPoint)) {
                    polygonPoints.add(lineFirstPoint);
                }
            }
        }
        return createPolygon(new ArrayList<>(polygonPoints));
    }

    private static boolean isPointBelongsToPolygon(Polygon polygon, Point point) {
        Line ray = new Line(point, new Point(Integer.MAX_VALUE, 0));
        int intersections = 0;
        for (int i = 0; i < polygon.getLines().size(); i++) {
            Line line = polygon.getLines().get(i);
            if (getLineIntersection(line, ray) != null) {
                intersections++;
            }
        }
        return intersections % 2 == 1;
    }
}

class Polygon {
    private final List<Line> lines;

    public Polygon(List<Line> lines) {
        this.lines = new ArrayList<>(lines);
    }

    public List<Line> getLines() {
        return lines;
    }
}

class Triangle {
    private final Line l1;
    private final Line l2;
    private final Line l3;

    public Triangle(Line l1, Line l2, Line l3) {
        this.l1 = l1;
        this.l2 = l2;
        this.l3 = l3;
    }

    public Line getL1() {
        return l1;
    }

    public Line getL2() {
        return l2;
    }

    public Line getL3() {
        return l3;
    }

    public double getArea() {
        double p = getPerimeter() / 2;
        return Math.sqrt(p * (p - l1.getLength()) * (p - l2.getLength()) * (p - l3.getLength()));
    }

    private double getPerimeter() {
        return l1.getLength() + l2.getLength() + l3.getLength();
    }
}

class Line {
    private final Point p1;
    private final Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }

    public double getLength() {
        return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
    }
}

class Point {
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
